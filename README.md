This is a small python file to access a Nextcloud notes instance and create, read, update and delete notes.

Configuration takes place in your home directory, under `.config/notes/config`

Here's an example config :
```use_https
host=test.org
username=password
password=password```