#!/usr/bin/python3

import requests
import sys
import os
import json 
from os.path import expanduser

host = None
baseRequest = "index.php/apps/notes/api/v0.2/"
use_https = False
username = None
password = None
url = None

def getNotes() :
    if url != None :
        r = requests.get(url + "notes")
        i = 0
        for note in r.json() :
            i += 1
            print("\n")
            print("Note " + str(i) + ", ID : " + str(note['id']))
            print(note['content'])

def getNote(id) :
    if url != None :
        r = requests.get(url + "notes/" + str(id))
        if r.status_code != 404 :
            return r.json()['content']
        else :
            return None

def modifyNote(id) :
    fileDesc = '/tmp/note-' + id
    with open(fileDesc, 'w+') as f :
        noteContent = getNote(id)
        if noteContent == None :
            print("status = 404, please check note ID provided is correct")
            pass

        f.write(noteContent)
        f.seek(0, 0)

        editor = input("Please enter the command for the editor you want to use : ")
        print("Opening file " + fileDesc + " ...")
        exitCode = os.system(editor + " " + fileDesc)
        if exitCode != 0 :
            print("editor returned -1, editing file may have failed, syncing anyway")

        # TODO close file here + reopen it to take changes in account

        with(open(fileDesc, 'r')) as read :
             noteContent = read.read()

             newURL = url + "notes/" + str(id)
             r = requests.put(newURL, data={'content':noteContent})
             print(r.status_code)

def readConfig() :
    global baseRequest
    global use_https
    global url
    #read config in user home
    with open(expanduser("~") + "/.config/notes/config") as configFile :
        while True :
            line = configFile.readline()
            if not line :
                break
            else :
                pairs = line.split("=")
                if(pairs[0] == "host") :
                    host = pairs[1]

                    #add / if there isn't one already
                    if(host[len(host) - 1] != "/") :
                        baseRequest = "/" + baseRequest

                elif(pairs[0] == "username") :
                    username = pairs[1]

                elif(pairs[0] == "password") :
                    password = pairs[1]

                elif(pairs[0] == "use_https") :
                    use_https = True
        url = "https://" + username + ":" + password + "@" + host + baseRequest
        url = url.replace("\n", "")
    pass

readConfig()
if(sys.argv[1] == "list") :
    getNotes()
elif (sys.argv[1] == "view") :
    noteContent = getNote(sys.argv[2])
    if noteContent != None :
        print(noteContent)
    else :
        print("status = 404, please check the provided note id is correct")
elif(sys.argv[1] == "modify") :
    modifyNote(sys.argv[2])
