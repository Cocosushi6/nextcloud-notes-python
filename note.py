import requests

class Note:

    def __init__(
        self,
        id=-1,
        content='',
        favorite=False,
        category=None,
        ):
        self.id = id
        self.content = content
        self.favorite = favorite
        self.category = category


class Profile:

    def __init__(
        self,
        serverAddress,
        username,
        password,
        use_https,
        ):
        self.serverAddress = serverAddress
        self.username = username
        self.password = password
        self.use_https = use_https
        if serverAddress[len(serverAddress) - 1] != '/':
            serverAddress += '/'

        self.url = 'https://' + username + ':' + password + '@' + serverAddress + 'index.php/apps/notes/api/v0.2/'
        self.url = self.url.replace('\n', '')

    def getNote(self, id):
        if self.url != None and id != None:
            r = requests.get(url + 'notes/' + str(id))
            if r.status_code != 404:
                response = r.json()
                note = Note(response['id'], response['content'],
                            response['favorite'], response['category'])
                return note
            else:
                return None

    def getNotes(self):
        notes = []
        if self.url != None:
            r = requests.get(self.url + 'notes')
            for note in r.json():
                newNote = Note(note['id'], note['content'],
                               note['favorite'], note['category'])
                notes.append(newNote)
        return notes

    def updateNote(self, id, content, favorite=False, category=None) :
        newURL = self.url + 'notes/' + str(id)
        r = requests.put(newURL, data={'content': content,
                         'favorite': favorite,
                         'category': str(category)})
        return r.status_code

    def newNote(self, note):
        newURL = self.url + 'notes/'
        r = requests.post(newURL, data={'content': note.content,
                          'favorite': note.favorite,
                          'category': note.category})
        if r.status_code != 404:
            return r.json()['id']
        else:
            return None

    def deleteNote(self, note):
        r = requests.delete(self.url + 'notes/' + str(note.id))
        return r.status_code
